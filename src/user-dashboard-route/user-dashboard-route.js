import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../visor-usuario/visor-usuario.js';
import '../login-usuario/login-usuario.js';
import '../visor-cuenta/visor-cuenta.js';
import '../logout-usuario/logout-usuario.js';
import '../alta-cuenta/alta-cuenta.js';
import '@polymer/iron-pages/iron-pages.js'
import '@polymer/iron-selector/iron-selector.js'
import '@polymer/app-route/app-route.js'
import '@polymer/app-route/app-location.js'

/**
* @customElement
* @polymer
*/
class UserDashboard extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h1>LOGIN de Usuario</h1>
     <select value="{{page::change}}">
       <option>Seleccionar componente</option>
       <option value="login">Login Usuario</option>
       <option value="alta">Alta Nuevo Usuario</option>
       <option value="logout">Logout Usuario</option>
       <option value="altacuenta">Alta Cuenta</option>
     </select>

     <app-location
         route="{{route}}"
         url-space-regex="^[[rootPath]]">
     </app-location>

     <app-route
      route="{{route}}"
      pattern="[[rootPath]]:page"
      data="{{routeData}}">
     </app-route>

           <iron-selector selected="[[page::changed]]" attr-for-selected="name" class="drawer-list" role="navigation"><br>
             <a hidden$="{{isLogged}}" name="login" href="[[rootPath]]login-usuario">Entra en tu banco</a> <br>
             <a hidden$="{{isLogged}}" name="alta-usuario" href="[[rootPath]]alta-usuario">Date de alta</a> <br>
             <a hidden$="{{!isLogged}}" name="usuario" href="[[rootPath]]visor-usuario">Tus datos</a> <br>
             <a hidden$="{{!isLogged}}" name="cuenta" href="[[rootPath]]visor-cuenta">Tus cuentas</a> <br>
             <a hidden$="{{!isLogged}}" name="logout" href="[[rootPath]]logout-usuario">Salir</a> <br>
           </iron-selector>

     <iron-pages
        selected="[[page]]"
        attr-for-selected="name"
        role="main">
      <login-usuario name="login" on-loginsucess="processLoginSuccess"></login-usuario>
      <visor-usuario name="usuario" userid={{userid}} on-eventocuentas="consultarCuentas"></visor-usuario>
      <visor-cuenta name="cuenta" userid={{userid}}></visor-cuenta>
      <alta-usuario name="alta"></alta-usuario>
      <logout-usuario name="logout" userid={{userid}}></logout-usuario>
      <alta-cuenta name="altacuenta" userid={{userid}}></alta-cuenta>
    </iron-pages>
    `;
  }

  static get properties() {
    return {

  //*    consultarCuentas2(e) {
  //*      console.log("funcion consultarCuenta");
  //*      this.balance = e.detail.idUser;
  //*      console.log("id:" + this.idUser);
  //*      this.page = "cuenta";
  //*    },

      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged',
      },
      isLogged: {
        type : Boolean,
        value : false,
        reflectToAttribute: true,
        observer: '_isLoggedChanged',
} ,
      route: Object,
      routeData: Object,
      subroute: Object,
// This shouldn't be neccessary, but the Analyzer isn't picking up
// Polymer.Element#rootPath
      rootPath: String,
    };
  }

  _isLoggedChanged(isLogged) {
    console.log("isLogged ha cambiado" + this.isLogged);

  }

  processLoginSuccess(e) {
    console.log("processLoginSuccess");
    console.log(e.detail.idUser);
    this.userid = e.detail.idUser;
    this.page = "usuario";

  }

  consultarCuentas(e) {
    console.log("funcion consultarCuenta");
    console.log(String(e.detail.idUser));
    this.userid = String(e.detail.idUser);
    console.log("id:" + this.iduser);
    this.page = "cuenta";
  }

  static get observers() {
  console.log("esstoy en observers");
  return [
    '_routePageChanged(routeData.page)',
    ];
  }

  _routePageChanged(page) {
    console.log("esstoy en _routePageChanged");
    // If no page was found in the route data, page will be an empty string.
    // Default to 'view1' in that case.
    console.log("this.page:" + this.page);
    console.log("page:" + page);
    this.page = page || 'inicio';

    // Close a non-persistent drawer when the page & route are changed.
    if (!this.$.drawer.persistent) {
      this.$.drawer.close();
    }
  }

  _pageChanged(page) {
    console.log("esstoy en _pageChanged");
    // Load page import on demand. Show 404 page if fails
    const resolvedPageUrl = this.resolveUrl('my-' + page + '.html');
    console.log("resolvedPageUrl:" + resolvedPageUrl);
    Polymer.importHref(
        resolvedPageUrl,
        null,
        this._showPage404.bind(this),
        true);
  }

  _showPage404() {
    this.page = 'view404';
  }

 }

 window.customElements.define("user-dashboard", UserDashboard);
