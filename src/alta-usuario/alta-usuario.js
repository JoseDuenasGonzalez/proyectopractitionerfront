import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
* @customElement
* @polymer
*/
class AltaUsuario extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <input type="email" placeholder="email" value="{{email::input}}" />
     <br />
     <input type="password" placeholder="password" value="{{password::input}}" />
     <br />
     <input type="first_name" placeholder="first_name" value="{{first_name::input}}" />
     <br />
     <input type="last_name" placeholder="last_name" value="{{last_name::input}}" />
     <br />
     <button on-click="alta">Alta</button>
     <span hidden$="[[!isAlta]]">Bienvenido a nuestro servicio de banca</span>


     <iron-ajax
       id="doAlta"
       url="http://localhost:3000/apitechu/v2/users"
       handle-as="json"
       content-type="application/json"
       method="POST"
       on-response="manageAJAXResponse"
       on-error="showError"
     >
     </iron-ajax>
   `;
 }
 static get properties() {
   return {
     password: {
       type: String
     }, email: {
       type: String
     }, isAlta: {
       type: Boolean,
       value: false
     }, first_name: {
       type: String
     }, last_name: {
       type: String
     }
   };
 } // End properties

 alta() {
   console.log("El usuario ha pulsado el botón");

   var altaData = {
     "email": this.email,
     "password": this.password,
     "first_name": this.first_name,
     "last_name": this.last_name
   }

   console.log("altaData " + this.email)

   this.$.doAlta.body = JSON.stringify(altaData);
   this.$.doAlta.generateRequest();
 }

 manageAJAXResponse(data) {
   console.log("Llegaron los resultados");
   console.log(data.detail.response);
   console.log(data.detail.response.email);

   this.isAlta = true;


 }

 showError(error) {
   console.log("Hubo un error");
   console.log(error);
   console.log(error.detail.request.xhr.response);
 }
} // End class

window.customElements.define('alta-usuario', AltaUsuario);
