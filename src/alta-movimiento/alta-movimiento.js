import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
* @customElement
* @polymer
*/
class AltaMovimiento extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <input type="type" placeholder="type" value="{{type::input}}" />
     <br />
     <input type="date" placeholder="date" value="{{date::input}}" />
     <br />
     <input type="amount" placeholder="amount" value="{{amount::input}}" />
     <br />
     <button on-click="alta">Confirmar movimiento</button>
     <span hidden$="[[!isAlta]]">Movimiento realizado correctamente</span>
          <button on-click="volverMovimientos">Volver a Movimientos</button>


     <iron-ajax
       id="doAlta"
       url="http://localhost:3000/apitechu/v2/movements"
       handle-as="json"
       content-type="application/json"
       method="POST"
       on-response="manageAJAXResponse"
       on-error="showError"
     >
     </iron-ajax>
   `;
 }
 static get properties() {
   return {
     iban: {
       type: String
     }, isAlta: {
       type: Boolean,
       value: false,
       observer: '_isAltaChanged',
     }, type: {
       type: String
     }, amount: {
       type: Number
     }, amount: {
       type: String
     }
   };
 } // End properties

 alta() {
   console.log("El usuario ha pulsado el botón");
   console.log("iban: " + this.iban)

   var altaData = {
     "IBAN": this.iban,
     "movement_type": this.type,
     "date": this.date,
     "amount": this.amount
   }

   console.log("altaData " + this.altaData)

   this.$.doAlta.body = JSON.stringify(altaData);
   this.$.doAlta.generateRequest();
 }

 manageAJAXResponse(data) {
   console.log("Llegaron los resultados");
   console.log(data.detail.response);
   console.log(data.detail.response.altaData);

   this.isAlta = true;


 }

 showError(error) {
   console.log("Hubo un error");
   console.log(error);
   console.log(error.detail.request.xhr.response);
 }

 volverMovimientos(e) {
  console.log("volverMovimientos");
  this.isAlta = false;
  this.dispatchEvent(new CustomEvent("vueltamovimientos"));

 }

 _isAltaChanged(isAlta) {
   console.log("isAlta ha cambiado" + this.isAlta);

 }
} // End class

window.customElements.define('alta-movimiento', AltaMovimiento);
