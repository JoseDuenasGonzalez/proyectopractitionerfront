import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../visor-usuario/visor-usuario.js';
import '../login-usuario/login-usuario.js';
import '../visor-cuenta/visor-cuenta.js';
import '../logout-usuario/logout-usuario.js';
import '../alta-cuenta/alta-cuenta.js';
import '../visor-movimientos/visor-movimientos.js';
import '../alta-movimiento/alta-movimiento.js';
import '@polymer/iron-pages/iron-pages.js'
import '@polymer/iron-selector/iron-selector.js'
import '@polymer/app-route/app-route.js'
import '@polymer/app-route/app-location.js'

/**
* @customElement
* @polymer
*/
class UserDashboard extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h1>LOGIN de Usuario</h1>
     <select value="{{page::change}}">
       <option>Seleccionar componente</option>
       <option hidden$="{{isLogged}}" value="login">Login Usuario</option>
       <option hidden$="{{isLogged}}" value="alta">Alta Nuevo Usuario</option>
       <option hidden$="{{!isLogged}}" value="logout">Logout Usuario</option>
       <option hidden$="{{!isLogged}}" value="altacuenta">Alta Cuenta</option>
     </select>


     <button hidden$="{{isLogged}}" on-click="showLogin">Login Cliente</button>
     <button hidden$="{{isLogged}}" on-click="showAlta">Alta Cliente</button>
     <button hidden$="{{!isLogged}}" on-click="showLogout">Logout</button>
     <button hidden$="{{!isLogged}}" on-click="showCuentas">Consultar cuentas</button>

     <iron-pages
        selected="[[page]]"
        attr-for-selected="name"
        role="main">
      <login-usuario name="login" on-loginsucess="processLoginSuccess"></login-usuario>
      <visor-usuario name="usuario" userid={{userid}} on-eventocuentas="consultarCuentas" on-eventoaltacuenta="altaCuenta"></visor-usuario>
      <visor-cuenta name="cuenta" userid={{userid}} flag={{flag}} on-eventomovimientos="consultarMovimientos" on-eventoaltacuenta="altaCuenta"></visor-cuenta>
      <alta-usuario name="alta"></alta-usuario>
      <logout-usuario name="logout" userid={{userid}}></logout-usuario>
      <alta-cuenta name="altacuenta" userid={{userid}} on-vueltacuentas="volver"></alta-cuenta>
      <visor-movimientos name="movimientos" iban={{iban}} flagmov={{flagmov}} on-altamovimiento="altaMovimiento"></visor-movimientos>
      <alta-movimiento name="pagealtamovimiento" iban={{iban}} on-vueltamovimientos="volverMov"></alta-movimiento>
    </iron-pages>
    `;
  }

  static get properties() {
    return {

  //*    consultarCuentas2(e) {
  //*      console.log("funcion consultarCuenta");
  //*      this.balance = e.detail.idUser;
  //*      console.log("id:" + this.idUser);
  //*      this.page = "cuenta";
  //*    },

      page: {
        type: String,
        reflectToAttribute: true,
      },
      isLogged: {
        type : Boolean,
        value : false,
        reflectToAttribute: true,
        observer: '_isLoggedChanged',
      },
      flag: {
        type : Number,
        value : +0,
      },
      flagmov: {
        type : Number,
        value : +0,
      }
    };
  }

  _isLoggedChanged(isLogged) {
    console.log("isLogged ha cambiado" + this.isLogged);

  }

  showLogin(e) {
    console.log("showLogin");
    this.page = "login";

  }

  showAlta(e) {
    console.log("showAlta");
    this.page = "alta";

  }

  showLogout(e) {
    console.log("showLogout");
    console.log(this.userid);
    this.isLogged = false;
    console.log("isLogged:" + this.isLogged);
    this.page = "logout";

  }

  showCuentas(e) {
    console.log("showCuentas");
    console.log(this.userid);
    console.log("isLogged:" + this.isLogged);
    this.page = "cuenta";

  }

  processLoginSuccess(e) {
    console.log("processLoginSuccess");
    console.log(e.detail.idUser);
    this.isLogged = true;
    console.log("isLogged:" + this.isLogged);
    this.userid = e.detail.idUser;
    this.page = "usuario";

  }

  consultarCuentas(e) {
    console.log("funcion consultarCuenta");
    console.log("id:" + this.userid);
    this.page = "cuenta";
  }

  consultarMovimientos(e) {
    console.log("funcion consultarMovimientos");
    console.log("iban:" + e.detail.IBAN);
    this.iban = e.detail.IBAN;
    this.page = "movimientos";
  }

  altaCuenta(e) {
    console.log("funcion altaCuenta");
    console.log(String(this.userid));
    this.page = "altacuenta";
  }

  altaMovimiento(e) {
    console.log("funcion altaMovimiento");
    console.log(String(this.iban));
    this.page = "pagealtamovimiento";
  }

  volver(e) {
    console.log("funcion volver");
    this.flag += 1;
    console.log("flag:" + this.flag);
    this.page = "cuenta";
  }

  volverMov(e) {
    console.log("funcion volverMov");
    this.flagmov += 1;
    console.log("flagmov:" + this.flagmov);
    this.page = "movimientos";
  }

 }

 window.customElements.define("user-dashboard", UserDashboard);
